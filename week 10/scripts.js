/*
    This is a JavaScript comment. You can type anything you like in this area,
    and it will be ignored. Enter your name and the date you completed this
    assignment in the space provided below. Be sure to push your work to 
    Bitbucket and submit the commit URL in the Blackboard assignment.
    
    Name:
    Date Complete:

*/
var canvas = document.querySelector("canvas");
var surface = canvas.getContext("2d");
var jack = document.getElementById("pumpkin");
var x = 0 
var y = 0
var direction = 1;

function moveIt() {
    surface.fillStyle = 'white';
    surface.fillRect(0,0,0,0);
    surface.drawImage(jack,x,y);

    if(x > 500 || x < 0) {
        direction = -direction;
    }
    
    x = x + direction;
    y = y + direction;
    
    requestAnimationFrame(moveIt);
}

moveIt();
