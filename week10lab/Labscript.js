var c = document.getElementById("dogpark");
var ctx = c.getContext("2d");

// create gradient
var grd=ctx.createLinearGradient(0,0,500,0);
grd.addColorStop(0,"green");
grd.addColorStop(1,"white");

// fill with gradient
ctx.fillStyle = grd;
ctx.fillRect(0,440,500,500);


var canvas = document.getElementById("dogpark");
var ctx = canvas.getContext("2d");
ctx.beginPath();
ctx.arc(100,75,50,0,2*Math.PI);
ctx.stroke();


var canvas = document.querySelector("canvas");
var surface = canvas.getContext("2d");
var pup = document.getElementById("dog");
var x = 0 
var y = 0
var direction = 1;

function moveIt() {
    surface.fillStyle = 'blue';
    surface.fillRect(0,0,0,0);
    surface.drawImage(pup,x,y);

    if(x > 500 || x < 0) {
        direction = -direction;
    }
    
    x = x + direction;
    y = y + direction;
    
    requestAnimationFrame(moveIt);
}

moveIt();
